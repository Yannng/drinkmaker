import com.google.common.base.Preconditions;

/**
 * Created by yoshi
 */
public class DrinkMakerManager {


    public static final String UNKNOWN_ORDER = "UNKNOWN_ORDER";
    public static final String UNKNOWN_DRINK = "UNKNOWN_DRINK";
    public static final String UNKNOWN_FORMAT = "UNKNOWN_FORMAT";

    public String convertToDrinkMakerFormat(Order order) {

        Preconditions.checkNotNull(order, UNKNOWN_ORDER);
        Preconditions.checkArgument(order.isValid(), UNKNOWN_DRINK);

        return buildDrinkMakertFormat(order);
    }

    public Message parseMessage(String drinkMakerMessage) {
        String delims = "[:,>]+";
        String[] splitedMessages = drinkMakerMessage.split(delims);

        Preconditions.checkArgument(splitedMessages.length == 2, UNKNOWN_FORMAT);
        Preconditions.checkArgument("<M".equals(splitedMessages[0]), UNKNOWN_FORMAT);

        Message message = new Message();
        message.setContenu(splitedMessages[1]);

        return message;
    }



    private String buildDrinkMakertFormat(Order order) {
        StringBuilder sb = new StringBuilder();
        sb.append("<")
                .append(order.getType())
                .append(":")
                .append(order.getSugarQuantity())
                .append(":")
                .append(order.getStick())
                .append(">");

        return sb.toString();
    }


}
