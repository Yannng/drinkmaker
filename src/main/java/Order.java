import java.io.Serializable;

/**
 * Created by yoshi
 */
public class Order {

    private String type ;
    private String sugarQuantity;
    private String stick;
    private boolean isValid;

    public Order() {
        super();
        type="";
        isValid = false;
        sugarQuantity = "";
        stick = "";
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {

        isValid = "C".equals(type)
                || "T".equals(type)
                || "H".equals(type);

        this.type = type;
    }

    public String getSugarQuantity() {
        return sugarQuantity;
    }

    public void setSugarQuantity(String sugarQuantity) {
        stick = ("0".equals(sugarQuantity)) ? "" : "0";
        this.sugarQuantity = sugarQuantity;
    }

    public boolean isValid() {
        return isValid;
    }

    public String getStick() {
        return stick;
    }

}
