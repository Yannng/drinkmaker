import junit.framework.Assert;
import org.junit.Test;

/**
 * Created by yoshi on 01/03/16.
 */
public class DrinkMakerTest {


    DrinkMakerManager drinkMakerManager = new DrinkMakerManager();

    @Test
    public void convertToDrinkMakerFormatTest() {

        Order order = new Order();
        order.setType("T");
        order.setSugarQuantity("1");
        Assert.assertEquals("<T:1:0>", drinkMakerManager.convertToDrinkMakerFormat(order));

        order = new Order();
        order.setType("T");
        order.setSugarQuantity("1");
        Assert.assertEquals("<T:1:0>", drinkMakerManager.convertToDrinkMakerFormat(order));


        order = new Order();
        order.setType("H");
        Assert.assertEquals("<H::>", drinkMakerManager.convertToDrinkMakerFormat(order));

        order = new Order();
        order.setType("H");
        Assert.assertEquals("<H::>", drinkMakerManager.convertToDrinkMakerFormat(order));


        order = new Order();
        order.setType("C");
        order.setSugarQuantity("2");
        Assert.assertEquals("<C:2:0>", drinkMakerManager.convertToDrinkMakerFormat(order));


    }

    @Test
    public void tryUnknownOrderExceptionTest() {
        // UNKNOWN_ORDER
        try {
            drinkMakerManager.convertToDrinkMakerFormat(null);
        } catch (NullPointerException npe) {
            Assert.assertEquals(npe.getMessage(), "UNKNOWN_ORDER");
        }
    }

    @Test
    public void tryUnknownDrinkExceptionTest() {
        // UNKNOWN_DRINK
        Order order = new Order();
        order.setType("L");
        try {
            drinkMakerManager.convertToDrinkMakerFormat(order);
        } catch (IllegalArgumentException iae) {
            Assert.assertEquals(iae.getMessage(), "UNKNOWN_DRINK");
        }
    }

    @Test
    public void parseMessageTest() {

        Message message = drinkMakerManager.parseMessage("<M:Bienvenu>");
        Assert.assertEquals(message.getContenu(), "Bienvenu");

    }

    @Test
    public void tryUnknownMessageFormatException() {

        try {
             drinkMakerManager.parseMessage("other");
        } catch (IllegalArgumentException iae) {
            Assert.assertEquals(iae.getMessage(), "UNKNOWN_FORMAT");
        }

        try {
            drinkMakerManager.parseMessage("<L:Hello>");
        } catch (IllegalArgumentException iae) {
            Assert.assertEquals(iae.getMessage(), "UNKNOWN_FORMAT");
        }


    }
}
